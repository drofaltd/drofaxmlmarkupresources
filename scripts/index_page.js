/*
* @Author: Drofa
* @Date:   2016-04-12 17:52:01
* @Last Modified by:   Vitaly Batushev
* @Last Modified time: 2016-05-20 13:54:13
* @Version  1.3
*/

$(function() {
    var tpl = '<span class="{{style}}{{color}}">{{num}}</span>',
        styleName = "bookMarkNum bookMarkNum-",
        colors = {blue: 0, green: 0, yellow: 0, red: 0};

    var localData = DrofaProperties.get(DrofaBookmarks.config.book.id) || "{}",
        bms = JSON.parse(localData),
        tpl = tpl.replace("{{style}}", styleName);

    for (var a = 0, links = $(Drofa.config.rootid + ".toc a"), l = links.length; a < l; a++) {
        var link = $(links[a]),
            tag = getChapterName(link),
            nextTag = getChapterName($(links[a + 1])),
            content = "";

        if (tag == nextTag) { continue; }

        for (var key in colors) { colors[key] = 0; }
        for (var key in bms) {
            var bm = bms[key];
            if (bm.chapter == tag) {
                colors[bm.clr]++;
            }
        }

        for (var key in colors) {
            if (colors[key] > 0) {
                content += tpl.replace("{{color}}", key).replace("{{num}}", colors[key]);
            }
        }

        link.html(content + link.html());
    }

    function getChapterName(link) {
        var href = link.attr("href");
        if (href === undefined) { return ""; }
        var arr = href.split("/");
        var name = arr[1] || arr[0];
        name = name.replace(/#.*$/gim, "");
        return name;
    }
})