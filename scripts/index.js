/*
* @Author: Drofa
* @Date:   2016-01-04 21:04:28
* @Last Modified by:   Vitaly Batushev
* @Last Modified time: 2016-04-06 19:31:46
* @Name Index.js
* @Version 2.0.2
*/

// События, происходящие после загрузки document
$(document).ready(function(){

/* вызов типографа typograph.js */
    if (typeof typograph == 'function') {
        typograph();
    }
/* typograph.js end */

    Drofa.tooltip();

    var coverBlock = Drofa.config.tpl.cover.replace(/{{suffix}}/gim, Drofa.config.doc.suffix);
    $('div.PageBreak[data-page-name="0"]').before(coverBlock);

    $("a").each(function(){
        $(this).attr("rel","external");
    });

    // удаление пустых страниц
    $('.PageBreak').each(function(){
        if ($(this).next().hasClass("PageBreak")) {
            $(this).remove();
        }
    });

    if (typeof DrofaSlideshow != "undefined") {
        DrofaSlideshow.init();
    }

    if (typeof DrofaToolbar != "undefined") {
        DrofaToolbar.init({
            chaptersFolderName: "chapters",
            xhtml: Drofa.config.doc.xhtml
        });
    }

    if (typeof DrofaPages != "undefined") {
        DrofaPages.draw();
    }

    if (typeof DrofaBookmarks != "undefined") {
        DrofaBookmarks.init();
    }

});

// События, приосходящие после загрузки window
window.addEventListener('load', function() {
    Drofa.initEmbedImgsSizes();
    Drofa.open.win();
    Drofa.icons.correct(false);
});

// События, приосходящие после изменения размера window
$(window).resize(function(){
    Drofa.icons.correct(true);
    if (typeof DrofaPages != "undefined") {
        DrofaPages.draw();
    }
});