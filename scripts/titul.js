/*
* @Author: vbatushev
* @Date:   2015-07-31 13:21:08
* @Last Modified by:   vbatushev
* @Last Modified time: 2015-07-31 13:21:44
*/

$(document).ready(function(){ titulBottom(); });
$(window).resize(function(){ titulBottom(); });
function titulBottom() {
    var bodyHeight = $("body").height();
    var topHeight = $("#titulTop").height();
    var bottomTop = $("#titulBottom").position().top;
    var bottomHeight = bottomTop + $("titulBottom").height();
    if (bottomHeight > bodyHeight) {
        $("#titulBottom").css({
            "position": "relative",
            "bottom": "",
            "left": "",
            "right": "",
        });
    } else {
        $("#titulBottom").css({
            "position": "absolute",
            "bottom": 0,
            "left": 0,
            "right": 0,
        });
    }
    if ($("#titulBottom").position().top < (topHeight - 5)) {
        $("#titulBottom").css({
            "position": "relative",
            "bottom": "",
            "left": "",
            "right": "",
        });
    }
}
