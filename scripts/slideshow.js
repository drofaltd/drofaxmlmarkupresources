/*
* @Author: Drofa
* @Date:   2016-01-04 21:04:28
* @Last Modified by:   Vitaly Batushev
* @Last Modified time: 2016-05-24 19:01:28
* @Name Слайдшоу и всплывающие изображения + добавление родительсокго класса во всплывающую картинку
* @Version 3.1
*/

/**
 * jQuery-селектор с использованием регулярного выражения
 * ПРИМЕР:
 * Выбрать все DIV атрибут class которых содержит цифры:
 * $('div:regex(class,[0-9])');
 */
jQuery.expr[":"].regex = function(elem, index, match) {
    var matchParams = match[3].split(","),
    validLabels = /^(data|css):/,
    attr = {
        method: matchParams[0].match(validLabels) ? matchParams[0].split(":")[0] : "attr",
        property: matchParams.shift().replace(validLabels,"")
    },
    regexFlags = "ig",
    regex = new RegExp(matchParams.join("").replace(/^\s+|\s+$/g,""), regexFlags);
    return regex.test(jQuery(elem)[attr.method](attr.property));
}

/**
 * Модуль показа слайдшоу
 * @property {Object} config  Объект конфигурации
 * @method            init    Разметить слайдшоу и изображения
 */
var DrofaSlideshow = (function(){
    var config = {
        root: Drofa.config.rootid || "#RootDiv",
        galleryConfig: {
            download: false,
            zoom: true,
            counter: false,
            closable: true,
            hideBarsDelay: 600000,
            scale: 0.5,
            zoomIconsIntoolbar: false,
            mwheel: true,
            limitzoom: 10,
            dragsliderzoom:true
        },
        slider: "slide-show",
        tpl: {
            gallery: {
                wrap: "<div id=\"galerystart\"></div>",
                item: "<a href=\"{item}\"></a>",
            },
        }
    };

    var main = function() {

        /**
         * Назначение события click для тегов IMG
         */
        $("img").on("click", function(e) {
            var $img = $(this);
            if(
                ($img.parents("p").length === 0 || $img.hasClass("zoom")) &&
                (!$img.hasClass("icon-in-fields") && !$img.hasClass("no-zoom") &&
                !$img.parents("*:regex(class, no-zoom)").length)
            ){
                ImageBox(e.target);
            }
        });

        var rootSlider = "div." + config.slider + ":not(:has(div." + config.slider + "))";

        $(rootSlider).each(function(index) {
            var out = "", num = index;
            var rel = "slideshow" + num;
            var containers = $(this).children();
            $(this).removeAttr("class").attr("class", config.slider);
            var parss = $(this);
            $.each(containers, function(index) {
                if (this.nodeName.toLowerCase() == "img") {
                    $(this).addClass("SlideShowImage");
                    out = $("<a class=\"SlideShowItem\" rel=\"" + rel + "\" href=\"" + $(this).attr("src") + "\">" + this.outerHTML + "</a>");
                    $(this).remove();
                } else {
                    var img = $(this).find("img")[0];
                    $(img).addClass("SlideShowImage");
                    out = $("<a class=\"SlideShowItem\" rel=\"" + rel + "\" href=\"" + $(img).attr("src") + "\" >" + img.outerHTML + "<div class=\"SlideShowCaption\"></div></a>");
                    var ssc = out.find(".SlideShowCaption");
                    $(this).find("p").each(function(){
                        ssc.html(ssc.html() + $(this).html());
                    });
                    out.attr("title", ssc.html());
                }
                parss.append(out);
                $(this).remove();
            });

            $("[rel=\"" + rel + "\"]").fancybox({
                locale: "ru",
                caption: {
                    type: "inside"
                },
                helpers: {
                    thumb: true
                },
                minHeight: 20,
                locales: {
                    "ru": {
                        CLOSE: "Закрыть",
                        NEXT: "Следующее изображение",
                        PREV: "Предыдущее изображение",
                        ERROR: "Запрошенное содержимое не может быть загружено. <br/> Пожалуйста, попробуйте позже.",
                        EXPAND: "Показать полное изображение",
                        SHRINK: "На всю ширину",
                        PLAY_START: "Начать показ слайдшоу",
                        PLAY_STOP: "Приостановить показ слайдшоу"
                    }
                }
            });

            $("a.SlideShowItem").on("click", function(e) {
                var parent = $(this).parent("div.owl-item");
                var rel = $(this).data("rel");
                return parent.hasClass("center");
            });
        });

        $(rootSlider).each(function(index) {
            var options = {
                items: 2,
                nav: true,
                dots: true,
                center: true,
                loop: true,
                autoWidth: true,
                margin: 40,
                navText: ["", ""]
            };
            var owl = $(this).owlCarousel(options);
            // Отключаю анимацию, чтобы изменение размера шрифта происходило моментально
            owl.on("translated.owl.carousel", function(event) {
                $(this).find(".owl-stage").css({transition:""});
            });

            owl.on("initialized.owl.carousel", function(event) {
                var owl_stage_outer = $(this).children(".owl-stage-outer");
                owl_stage_outer.each(function() {
                    var cap = $(event.target).find(".center .SlideShowCaption").html() || "";
                    $("<div id=\"owl-caption" + index + "\" class=\"owl-caption\">" + cap + "</div>").insertAfter($(this).parent().children(".owl-controls"));
                    var cur_owl_stage = $(this),
                        recalc_caption_height = function(event) {
                            var owl_caption = $("#owl-caption" + index),
                                initial_content = owl_caption.html(),
                                stage_width = owl_stage_outer.width(),
                                maxCaptionHeight = 0,
                                addHeight = 0;

                            cur_owl_stage.find(".SlideShowCaption").each(function() {
                                $(this).css("width", stage_width + "px");
                                maxCaptionHeight = Math.max(maxCaptionHeight, $(this).height());
                            });

                            // добавляю пробел, чтобы не сработал селектор :empty с нулевыми padding
                            if (initial_content == "") {
                                owl_caption.html(" ");
                            }
                            addHeight = Math.ceil(parseFloat(owl_caption.css("padding-top")) + parseFloat(owl_caption.css("padding-bottom")));
                            if (initial_content == "") {
                                owl_caption.html("");
                            }
                            if (maxCaptionHeight > 0) {
                                owl_caption.css("min-height", maxCaptionHeight + addHeight + "px");
                            }
                        };
                    $("body").on("stepfont-after", recalc_caption_height);
                    $(window).on("resize", recalc_caption_height);
                });

                $("div.cloned").on("click", function(e) {
                    return false;
                });

                $("div.cloned > a.SlideShowItem").on("click", function(e) {
                    return false;
                });
            });

            $(document).bind("cbox_closed", function() {
                owl.trigger("destroy.owl.carousel");
                owl = $(this).owlCarousel(options);
            });
            owl.on("changed.owl.carousel", function(event) {
                $("#owl-caption" + index).html($(event.target).find(".center .SlideShowCaption").html());
            });
            owl.on("translated.owl.carousel", function(event) {
                $("#owl-caption" + index).html($(event.target).find(".center .SlideShowCaption").html());
            });

        });

        $("img.SlideShowImage").each(function() {
            var $img = $(this);
            $img.css({ width: "100%" });
            var parentHeight = $img.parent().parent("div").height();
            if ($img.height() < parentHeight) {
                $img.css({ height: "100%", width: "auto" });
            }
        });
    }

    /**
     * Обработка элемента DOM с тегом IMG
     * @param {DOMElement} item Обрабатываемый элемент DOM
     */
    var ImageBox = function(item) {
        var scrollV, scrollH,
            itemClone = $(item).clone(),
            itemClass = $(item).attr("class");

        var galerystart = $(config.tpl.gallery.wrap);
        galerystart.append($(config.tpl.gallery.item.replace("{item}", item.src)).append(itemClone));
        var galery = galerystart.lightGallery(config.galleryConfig)
            .on("onAfterOpen.lg",function(event) {
                $(".lg-toolbar").remove();
                scrollV = $(window).scrollTop();
                scrollH = $(window).scrollLeft();
                $("html").addClass("fancybox-lock");
                $(window).scrollTop(scrollV).scrollLeft(scrollH);
                $("<div class=\"close-zoom-block\"></div>").prependTo($(".zoom-block")).on("click", function() {
                    galery.data("lightGallery").destroy();
                });
                galery.data("lightGallery").$slide.find(".lg-object").addClass(itemClass);
            })
            .on("onBeforeClose.lg",function(event) {
                $("html").removeClass("fancybox-lock");
                $(window).scrollTop(scrollV).scrollLeft(scrollH);
                galery.data("lightGallery", null);
            });

        galery.data("lightGallery").index = 0;
        galery.data("lightGallery").build(0);
        $("body").addClass("lg-on");

        $(window).on("keydown", function(e) {
            if (e.keyCode === 27) {
                e.preventDefault();
                if (galery.data("lightGallery")) {
                    galery.data("lightGallery").destroy();
                }
            }
        });
    }

    return {
        init: main,
        config: config
    }
})();
