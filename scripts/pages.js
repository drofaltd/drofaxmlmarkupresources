/*
* @Author: Drofa
* @Date:   2016-01-19 20:12:21
* @Last Modified by:   Vitaly Batushev
* @Last Modified time: 2016-05-24 14:32:00
* @Name Расстановка страниц
* @Version 1.0.14
*/

/**
 * Модуль расстановки страниц
 * @param    config  Конфигурация модуля
 * @method   draw    Отображение страниц
 */
var DrofaPages = (function(){
    var config = {
        container: {
            tag: "div",
            id: "PageBreakOuter"
        },
        tpl: {
            id: "BookPageNum",
            class: "PageNum",
            page: "<div class=\"{{class}}\" data-page=\"{{num}}\" id=\"{{id}}{{num}}\">{{num}}</div>",
        },
        lastID: "modal-box",
        pagebreak: {
            class: "page-break",
            dataname: "data-page-name",
        },
        offsets: {
            icon: 50,
        }
    }
    var container;

    /**
     * Отрисовка страниц в создаваемом контейнере
     */
    var main = function() {
        var pages = $("." + config.pagebreak.class);
        var iconfields = $("a.icon-in-fields");

        var offseticon = config.offsets.icon;
        if (typeof curSize !== "undefined") {
            offseticon = offseticon * curSize;
        }

        createContainer();
        for (var a = 0, l = pages.length; a < l; a++) {
            var el = $(pages[a]),
                pagename = el.attr(config.pagebreak.dataname),
                nextid = "", hide = false;

            if (typeof pages[a + 1] !== "undefined") {
                nextid = pages[a + 1].id;
            }
            if (pagename > 0 && nextid !== config.lastID) {
                var pagepoint = $("#" + config.tpl.id + pagename);
                if (!pagepoint.length) {
                    var page = $(config.tpl.page
                                .replace(/{{num}}/gim, pagename)
                                .replace(/{{id}}/gim, config.tpl.id)
                                .replace(/{{class}}/gim, config.tpl.class)
                        );
                    $(container).append(page);
                }
            }
            var tp = el.offset().top;

            // Смещение от иконок вниз
            var icon_down = tp;
            for (var d = 0; d < iconfields.length; d++) {
                var icon = $(iconfields[d]),
                    topicon = icon.offset().top,
                    hicon = icon.outerHeight(true);
                if (icon_down + offseticon > topicon && icon_down < topicon + hicon) {
                    icon_down = topicon + hicon;
                } else {
                    if (icon_down + offseticon < topicon) {
                        break;
                    }
                }
            }

            // Смещение от иконок вверх
            var icon_up = tp;
            for(var u = iconfields.length - 1; u > -1; u--) {
                var icon = $(iconfields[u]),
                    topicon = icon.offset().top,
                    hicon = icon.outerHeight(true);
                if (icon_up + offseticon > topicon && icon_up < topicon + hicon) {
                    icon_up = topicon - offseticon;
                } else {
                    if (icon_up > topicon) {
                        break;
                    }
                }
            }

            if (tp - icon_up < icon_down - tp) {
                tp = icon_up;
            } else {
                tp = icon_down;
            }

            if (tp < $("nav").outerHeight(true) + 25) {
                tp = $("nav").outerHeight(true) + 25;
            }
            $("#" + config.tpl.id + pagename).css("top", tp);
            if (hide) {
                $("#" + config.tpl.id + pagename).text("");
            }
        }

        /* Убираем номера страниц, расположенные слишкои близко друг к другу (предварительно сортируем номера по их расположению)*/
        var arr = [];
        $("." + config.tpl.class).each(function() { arr.push(this); })
        arr.sort(function(el1, el2) {
            var top1 = parseInt(el1.style.top, 10);
            var top2 = parseInt(el2.style.top, 10);
            if (top1 == top2) return 0;
            if (top1 < top2) return -1;
            return 1;
        })

        for (var a = arr.length - 1; a > 0; a--) {
            if (parseInt(arr[a].style.top, 10) - parseInt(arr[a - 1].style.top, 10) < 150) {
                $(arr[a]).remove();
            }
        }
    }

    /**
     * Создание контейнера для страниц
     */
    var createContainer = function() {
        container = document.getElementById(config.container.id);
        if (container === null) {
            delete container;
            var box = document.createElement(config.container.tag);
            box.id = config.container.id;
            container = document.body.appendChild(box);
        }
    }

    return {
        draw: main,
        config: config
    }
})();