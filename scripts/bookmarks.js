/*
* @Author: Drofa
* @Date:   2016-04-11 15:48:06
* @Last Modified by:   Vitaly Batushev
* @Last Modified time: 2016-06-01 15:45:43
* @Version 3.3.1
*/

/**
 * Закладки
 * @method    .fn.*                Публичные функции, которые могут быть вызваны и переопределены извне
 * @method    .fn.saveBookmark     Сохранение закладки в Storage или на сервере
 * @method    .fn.removeBookmark   Удаление закладки в Storage или на сервере
 * @method    .fn.getBookmarks     Получение всех закладок с сервера или из Storage
 * @method    .gets                Синоним .fn.getBookmarks, оставлен для совместимости
 * @method    .getbookid           Получение ID книги
 * @method    .mark                Расстановка закладок
 * @method    .init                Инициализация DrofaBookmarks
 * @param     {Object} config      Коллекция переопределяемых переменных
 */

var DrofaBookmarks = (function(){
    var $, fn = {}, bm = {}, CurrentBookmark = {};
    if (typeof jQuery !== "undefined") {
        var $ = jQuery;
    } else {
        console.log("Не обнаружен jQuery");
        document.head.appendChild("<script src=\"//code.jquery.com/jquery-2.1.4.min.js\"></script>");
    }

    var config = {
        book: {},                                                           // Данные о книге
        rootid: Drofa.config.rootid || "#RootDiv",                          // ID корневого элемента (не body)
        chaptersFolderName: Drofa.config.chaptersFolderName || "chapters",  // Наименование папки глав
        corrector: 14,                                                      // Поправка к положение по вертикали
                                                                            // при нажатии click или top
        topCorrect: 117,                                                    // высота между кнопкой закрытия и закладкой
        notfound: "notFoundBookmarks",                                      // ID для ненайденных закладок
        formid: "#BookmarkForm",                                            // ID формы редактирования закладки
        wrapper: "div.wrapbookmark",                                        // Элемент обертки закладок
        popoverclass: ".popover",                                           // Класс всплывающих элементов
        suffix: Drofa.config.doc.suffix || ".xhtml",                        // Расширение страниц
        xhtml: Drofa.config.doc.xhtml || true,                              // XHTML или HTML
        overshifts: {                                                       // Поправки при скроллинге к окну редактирования закладки
            windows: -0.3,                                                  // для декстопных браузеров
            mobile: -0.3,                                                   // для мобильных браузеров
            ios: -0.1,                                                      // для браузеров в iOS
        },
        link: {
            success: true,                                                  // Состояние связи с сервером
        },
        events: {
            click: setEventClick(),                                         // Определение события click или tap
        },
        tpl: {                                                              // Шаблоны
            // Окно редактирования закладки
            bookmark: '\
                <form id="BookmarkForm">\
                    <button class="btn-close" type="reset"><i class="fa fa-times"></i></button>\
                    <input type="hidden" id="BookmarkEdit" value="0" />\
                    <div id="EditBookmarkToolbar">\
                        <button type="button" class="btn btn-remove" id="BookmarkRemove"></button>\
                        <div id="EditBookmarkColors" class="btn-group" data-toggle="buttons">\
                            <div class="shadow"></div>\
                            <label class="btn btn-blue active">\
                                <input onchange="DrofaBookmarks.color(this)" id="colorb" type="radio" name="color" value="blue" checked="checked" /><i class="fa fa-bookmark"></i>\
                            </label>\
                            <label class="btn btn-yellow">\
                                <input onchange="DrofaBookmarks.color(this)" id="colory" type="radio" name="color" value="yellow" /><i class="fa fa-bookmark"></i>\
                            </label>\
                            <label class="btn btn-red">\
                                <input onchange="DrofaBookmarks.color(this)" id="colorr" type="radio" name="color" value="red" /><i class="fa fa-bookmark"></i>\
                            </label>\
                            <label class="btn btn-green">\
                                <input onchange="DrofaBookmarks.color(this)" id="colorg" type="radio" name="color" value="green" /><i class="fa fa-bookmark"></i>\
                            </label>\
                        </div>\
                        <button class="btn btn-add" id="BookmarkSubmit">Сохранить закладку</button>\
                    </div>\
                    <div id="EditBookmarkContent">\
                        <textarea rows="3" id="BookmarkMsg" class="bmproperty" name="msg"></textarea>\
                        <div class="checkbox pull-right"><label><input type="checkbox" value="0" id="BookmarkLater" class="bmproperty" name="later" /> Позже</label></div>\
                    </div>\
                    <div id="EditBookmarkFootbar">\
                        <table class="table">\
                            <tr>\
                                <td class="user" width="80%"></td>\
                                <td class="date"></td>\
                                <td class="time"></td>\
                            </tr>\
                            <tr>\
                                <td class="creator" width="80%"></td>\
                                <td class="createdate"></td>\
                                <td class="createtime"></td>\
                            </tr>\
                        </table>\
                    </div>\
                </form>\
            ',
            // Полоса размещения закладок
            bar: "<div id=\"bookmarksBar\"></div>",
            // Элемент закладки на станице
            popup: "<div class=\"wrapbookmark\" id=\"{{BookmarkID}}\" data-toggle=\"popover\" data-placement=\"left\" data-target=\"#popupBookmark\"><div class=\"bookmark bookmark-blue\" title=\"\"></div></div>",
            // Данные
            data: {
                // Имя пользователя
                username: "<i class=\"glyphicon glyphicon-pencil\"></i> {{username}}",
                // Текущая дата
                date: "<i class=\"glyphicon glyphicon-calendar\"></i> {{date}}",
                // Текущее время
                time: "<i class=\"glyphicon glyphicon-time\"></i> {{time}}",
                // Имя создателя закладки
                creator: "<i class=\"glyphicon glyphicon-user\"></i> {{username}}",
                // Дата создания закладки
                createdate: "<i class=\"glyphicon glyphicon-calendar\"></i> {{date}}",
                // Время создания закладки
                createtime: "<i class=\"glyphicon glyphicon-time\"></i> {{time}}",
            },
            // Контейнер со списком закладок в верхнем меню
            container: '\
            <div role="menu" class="dropdown-menu dropdown-menu-right" id="MenuBookmarks">\
                <div id="MenuBookmarksToolbar">\
                    <a class="sortbtn btn fa fa-page" id="MenuBookmarksSortPorPage" data-sort="asc" data-type="page">&#160;</a>\
                    <a class="btn fa fa-check-square-o laterselect" data-state="{{filters.actual}}" id="SelectNoLater">&#160;</a><span>Сейчас</span>\
                    <a class="btn fa fa-check-square-o laterselect" data-state="{{filters.later}}" id="SelectLater">&#160;</a><span>Позже</span>\
                    <a class="sortbtn btn pull-right fa fa-date" id="MenuBookmarksSortPorDate" data-sort="asc" data-type="date">&#160;</a>\
                </div>\
                <div id="MenuBookmarksContainer">\
                </div>\
                <div id="MenuBookmarksFooter">\
                     <a class="btn fa fa-check-square-o laterselect" data-state="{{filters.count}}" id="BookmarkCounter" title="Показ количества закладок для каждой главы в содержании">&#160;</a><span  title="Показ количества закладок для каждой главы в содержании">Количество закладок</span>\
                </div>\
            </div>',
            // Элемент закладки в верхнем меню
            menuitem: '\
            <div class="bm {{laterclass}}" data-bm="{{id}}" id="menu{{id}}">\
                <a data-ajax="false" class="menuitem" href="{{path}}?#{{id}}">\
                    <table class="table">\
                        <tr>\
                            <td class="mark bookmark-{{color}}">{{pagename}}</td>\
                            <td class="user" title="{{username}}"><i class="glyphicon glyphicon-pencil"></i> {{usernametrim}}</td>\
                            <td class="date"><i class="glyphicon glyphicon-calendar"></i> {{date}}</td>\
                            <td class="time"><i class="glyphicon glyphicon-time"></i> {{time}}</td>\
                            <td class="remove"><div class="close" title="Удалить закладку" data-id="{{id}}"><i class="glyphicon glyphicon-remove"></i></div></td>\
                        </tr>\
                        <tr>\
                            <td class="">Создано:</td>\
                            <td class="user" title="{{creator}}"><i class="glyphicon glyphicon-user"></i> {{creatortrim}}</td>\
                            <td class="date"><i class="glyphicon glyphicon-calendar"></i> {{createdate}}</td>\
                            <td class="time"><i class="glyphicon glyphicon-time"></i> {{createtime}}</td>\
                        </tr>\
                        <tr>\
                            <td colspan="5" class="msg">{{msg}}</td>\
                        </tr>\
                    </table>\
                </a>\
            </div>',
            bookmarkMark: '<div class="wrapbookmark {{laterclass}}" id="{{id}}" data-toggle="popover"><div class="bookmark bookmark-{{color}}" title="{{msg}}" data-toggle="tooltip"></div></div>',
        }
    }

    /**
     * Функции для работы объекта DrofaBookmark
     */
    /**
     * Сохранение закладки
     */
    bm.save = function() {
        var bmdata = JSON.stringify(this);
        var data = DrofaProperties.get(DrofaBookmarks.config.book.id) || "{}";
        var bookmarks = JSON.parse(data);
        bookmarks[this.id] = JSON.parse(bmdata);
        var dataSave = {
            id: this.id,
            config: DrofaBookmarks.config,
            bmdata: bmdata,
            bookmarks: JSON.stringify(bookmarks)
        }
        fn.saveBookmark(dataSave);
    }

    /**
     * Удаление закладки
     */
    bm.remove = function() {
        var bookmarks = DrofaProperties.get(DrofaBookmarks.config.book.id) || "{}";
        var newbk = JSON.parse(bookmarks);
        delete newbk[this.id];

        $(DrofaBookmarks.config.formid).trigger("reset");
        $(DrofaBookmarks.config.wrapper).popover("destroy");
        $(DrofaBookmarks.config.popupclass).remove();

        var dataRemove = {
            id: this.id,
            config: DrofaBookmarks.config,
            bookmarks: JSON.stringify(newbk)
        }

        fn.removeBookmark(
            dataRemove,
            function() {
                DrofaBookmarks.mark();
            }
        );
    }

    /**
     * Публичные функции
    */

    /**
     * Сохранение закладки в localStorage или на сервер
     * @param  {Object} dataSave Объект с данным закладки
     */
    fn.saveBookmark = function(dataSave){
        DrofaProperties.set(DrofaBookmarks.config.book.id, dataSave.bookmarks);
        if (window.location.protocol !== "file:") {
            $.post(DrofaBookmarks.config.book.saveurl, {
                info: DrofaBookmarks.config.book.data.info,
                bookmark_id: dataSave.id,
                bookmark_data: dataSave.bmdata
            }, function(data) {
                if (data.status == "ok") {
                    console.log("Закладка сохранена.");
                } else {
                    alert("Не удалось сохранить закладку. " + data.comment);
                }
            });
        }
    };

    /**
     * Удаление закладки из localStorage или с сервера
     * @param  {Object}     dataRemove   Объект с данным закладки
     * @param  {Function}   onOK         Функция, выполняющаяся при благополучном завершении
     *                                   сохранения закладки на сервер
     */
    fn.removeBookmark = function(dataRemove, onOK) {
        DrofaProperties.set(DrofaBookmarks.config.book.id, dataRemove.bookmarks);
        if (window.location.protocol !== "file:") {
            $.post(DrofaBookmarks.config.book.delurl, {
                info: DrofaBookmarks.config.book.data.info,
                bookmark_id: dataRemove.id
            }, function(data) {
                if (data.status == "ok") {
                    if (onOK) { onOK(); }
                }
            });
        } else {
            if (onOK) { onOK(); }
        }
    };

    fn.showBookmarksWindow = function(id) {
        var menutop = $("#" + DrofaProperties.get(id));
        if (menutop.length != 0) {
            $("#MenuBookmarksContainer").scrollTop($("#MenuBookmarksContainer").scrollTop() + menutop.position().top);
        }
    }

    /**
     * Получение закладок и расстановка их на странице
     * @param  {String} id       ID книги
     * @param  {Object} bookdata Данные книги
     * @param  {String} url      URL запроса получения закладок
     */
    fn.getBookmarks = function(id, bookdata, url) {
        var id = DrofaBookmarks.config.book.id || id,
            bookdata = DrofaBookmarks.config.book.data || bookdata,
            url = DrofaBookmarks.config.book.geturl || url;

        if (window.location.protocol !== "file:") {
            var info = bookdata.info;
            $.post(url, {
                info: info
            }, function(data) {
                if (data.status == "ok") {
                    $("#alertConnection").removeClass("alert-danger").addClass("alert-success").html("<i class=\"glyphicon glyphicon-ok-circle\"></i>");
                    DrofaBookmarks.config.link.success = true;
                    if (data.bookmarks !== null) {
                        DrofaProperties.set(id, JSON.stringify(data.bookmarks));
                    }
                } else {
                    alert("Отсутствует соединение с сервером");
                    DrofaBookmarks.config.link.success = false;
                    $("#alertConnection").removeClass("alert-success").addClass("alert-danger").html("<i class=\"glyphicon glyphicon-ban-circle\"></i>");
                    DrofaProperties.set(id, "");
                }
            });
        } else {
            $("#alertConnection").removeClass("alert-success").addClass("alert-danger").html("<i class=\"glyphicon glyphicon-ban-circle\"></i>");
        }
        markBookmarks();
    }

    /**
     * Набор helper-ов
     *
     */
    var CommonBookmarks = function(){
        /**
         * Получение ID книги
         * @return {String} ID книги
         */
        function getBookID() {
            var id = "DrofaBM", data = getData();
            return id + data.object_id + "r" + data.object_rev;
        }

        /**
         * Получение имени главы
         * @return {String}     Имя главы
         */
        function getChapterName() {
            if (location.pathname.indexOf("/" + config.chaptersFolderName + "/") > -1) {
                var arr = location.pathname.split("/");
                var test = "", a = 0;
                for (var a = 0, l = arr.length; a < l; a++) {
                    if (arr[a] == config.chaptersFolderName) {
                        return arr[a + 1];
                    }
                }
            } else {
                return location.pathname.substr(location.pathname.lastIndexOf("/") + 1);
            }
        }

        /**
         * Получение имени страницы и пути к файлу
         * @param  {DrofaBookmark} bookmark     Объект DrofaBookmark
         * @return {Object}                     Объект, содержащий значения:
         *                                      .pagename — имя страницы в книге
         *                                      .path — путь к странице (X)HTML
         */
        function getPathAndName(bookmark) {
            var out = {path: "", pagename: bookmark.page + " с."};
            if (bookmark.page == "") {
                out.pagename = "";
            }
            var prefix = "";
            if (location.pathname.indexOf(config.chaptersFolderName) > -1) { prefix = "../../"; }
            if (location.pathname.indexOf("help") > -1) { prefix = "../"; }
            if (bookmark.chapter.indexOf(config.suffix) > -1) {
                var shortname = bookmark.chapter.replace(/\.x?html/im,""),
                    chaptername = getChapterName();
                if (shortname == "help") {
                    out.path = "help/";
                    out.pagename = "Помощь";
                    out.path += shortname + config.suffix;
                }
                out.path = prefix + shortname + config.suffix;

            } else {
                out.path = prefix + config.chaptersFolderName + "/" + bookmark.chapter + "/" + "index" + config.suffix;
            }
            return out;
        }

        /**
         * Определение координаты элемента по горизонтали
         * @param {Object}     elem     Элемент DOM
         * @return {Integer}            Позиция по горизонтали
         */
        function PageX(elem) {
            return elem.offsetParent ?
                elem.offsetLeft + PageX(elem.offsetParent) :
                elem.offsetLeft;
        }

        /**
         * Определение координаты элемента по вертикали
         * @param {Object}     elem     Элемент DOM
         * @return {Integer}            Позиция по вертикали
         */
        function PageY(elem) {
            var result = 0;
            if (elem !== null) {
                if (elem.offsetParent) {
                    result = elem.offsetTop + PageY(elem.offsetParent);
                } else {
                    result = elem.offsetTop;
                }
            }
            return result;
        }

        /**
         * Привязка позиции закладки к элементу DOM
         * Элементы DOM, к которым может быть привязана закладка:
         * H1-H6, P, IMG, TR. Элементы должны иметь ID.
         * Если подходящий элемент не найдет, в качестве "тега" используется значение calcFromBody
         * @param  {Integer} pos     Позиция закладки
         * @return {Object}          Объект, содержащий данные о привязке
         *                           .pos — позиция относительно высоты элемента DOM, к которому привязана закладка
         *                           .pagepos — позиция относительно страницы
         *                           .tag — тег с ID элемента DOM или calcFromBody
         *                           .page — страница книги
         *                           .correct — поправка ?
         */
        function withParagraph(pos) {
            var elems = $("h1[id],h2[id],h3[id],h4[id],h5[id],h6[id],p[id],img[id],tr[id]");
            var bodyOffset = $("body").offset();
            var anchor = document.body,
                topElem, bottomElem, pageValue,
                tag = "calcFromBody", offset = 0;

            for (var a = 0, l = elems.length; a < l; a++) {
                var current = elems[a];
                var b = current.id;
                topElem = PageY(current);
                bottomElem = topElem + current.offsetHeight + parseInt(window.getComputedStyle(current).marginBottom);
                if (a > 0) {
                    if (bottomElem > pos && pos > topElem) {
                        if (parseInt(current.offsetHeight) !== 0) {
                            anchor = current;
                        } else {
                            var z = i - 1;
                            do {
                                anchor = elems[a];
                                pos = (PageY(anchor) + parseInt(anchor.offsetHeight)) - 5;
                                z--;
                            } while(anchor.nodeName.toLowerCase() == "span");
                        }
                        tag = anchor.id;
                        break;
                    }
                }
            }

            var page, pagePos, nextPage, nextPagePos, nextPageValue,
                pages = $("div.page-break");

            for (var a = 0, l = pages.length; a < l; a++) {
                page = pages[a];
                pagePos = PageY(page);
                pageValue = page.dataset.pageName;
                nextPage = pages[a + 1];
                if (nextPage === undefined) {
                    nextPagePos = parseInt(window.getComputedStyle(document.body).height);
                    nextPageValue = parseInt(pageValue) + 1;
                } else {
                    nextPagePos = PageY(nextPage);
                    nextPageValue = nextPage.dataset.pageName;
                }
                if (nextPagePos > pos) {
                    break;
                }
            }

            if (pageValue === undefined) { pageValue = 0; }
            pageValue = +pageValue;

            var heightPara = parseInt(anchor.offsetHeight);
            topElem = CommonBookmarks.page.y(anchor);
            var diff = pos - topElem;
            offset = parseInt(diff / (heightPara / 100)).toFixed(2);
            var heightWin = parseInt(window.getComputedStyle(document.body).height);
            var pagepos = (pos / (heightWin / 100)).toFixed(2);
            var correct = pos - (parseInt(window.getComputedStyle(anchor).marginTop));

            return {
                "pos": offset,
                "pagepos": pagepos,
                "tag": tag,
                "page": pageValue,
                "correct": correct
            };
        }

        /**
         * "Уплощение" URL (в строке URL убираются двоеточия и слеши)
         * @return {String} "Сплющенная" строка
         */
        function urlFlatString() {
            var out = "", hrefs = location.href.split("/");
            for (var a = 0, l = hrefs.length - 1; a < l; a++) {
                if (hrefs[a] == "help" || hrefs[a] == config.chaptersFolderName || hrefs[a].indexOf("index" + Drofa.config.doc.suffix) > -1){
                    return out;
                }
                out += hrefs[a].replace(":", "");
            }
            return out;
        }

        /**
         * Получение данных о книге:
         *     .object_id     ID объекта книги
         *     .object_rev    Редакция книги
         *     .username      Имя пользователя
         *                    (всегда отдается как "Локальный пользователь",
         *                    если пользователь активирован на сайте, его имя меняется позднее)
         *     .info          Пустая строка
         *     .user_id       0
         * @return {[type]} [description]
         */
        function getData() {
            var oid = "", orev = "", i,
                lh = location.href.split("/");

            if (window.location.protocol !== "file:") {
                var host = location.href.replace(/^.*?binaries\//gim, "");
                lh = host.split("/");
                oid = lh[0];
                var n = lh[1].split("-");
                if (n.length > 1) {
                    orev = n[1];
                } else {
                    orev = n[0];
                }
                orev = orev.replace(/[A-z]/gim, "");
            } else {
                var a = urlFlatString();
                oid = md5(a);
            }

            var data = {
                object_id: oid,
                object_rev: orev,
                username: "Локальный пользователь",
                info: "",
                user_id: 0
            };

            var udata = DrofaProperties.get(window.location.hostname + "-data") || null;
            if (udata !== null) {
                user = JSON.parse(udata);
                data.username = user.username;
                data.info = user.info;
                data.user_id = user.user_id;
            }
            return data;
        }

        /**
         * [formatUrl description]
         * @param  {[type]} type [description]
         * @return {[type]}      [description]
         */
        function formatUrl(type) {
            var urlTemplate = "/apiObject/{type}{object}/id/{objectID}/rev/{objectRevision}/",
                object = "ObjectBookmark";
            if (type == "get") { object = "ObjectBookmarks"; }
            return urlTemplate.replace("{type}", type)
                              .replace("{object}", object)
                              .replace("{objectID}", config.book.data.object_id)
                              .replace("{objectRevision}", config.book.data.object_rev);
        }

        /**
         * Добавление нулей к цифрам
         * @param  {Number} num Анализируемое число
         * @param  {Number} pow Степень для числа 10,
         *                      если параметр опущен, степень равна 1
         * @return {String}     Сформированное число
         *                      (если num больше 10 в степени pow, возвращается num)
         */
        function preZero(num, pow) {
            var pow = pow || 1;
            var l = pow + 1;
            for (var a = 1; a < l; a++) {
                var point = Math.pow (10, a);
                if (num < point) {
                    return Array(l + 1 - a).join("0") + num;
                }
            }
            return num;
        }

        /**
         * Получение метки времени (timestamp)
         * @return {Number} Значение метки времени
         */
        function getNormalizeDate() {
            return (new Date()).valueOf();
        }

        /**
         * Преобразование метки времени (timestamp) в удобочитаемую строку
         * @param  {Number} value  Метка времени
         * @return {String}        Полученная строка (ДД.ММ.ГГ ЧЧ:ММ)
         */
        function dateConvert (value) {
            var date = new Date(value),
                day = CommonBookmarks.zero(date.getDate()),
                month = CommonBookmarks.zero(date.getMonth() + 1),
                hours = CommonBookmarks.zero(date.getHours()),
                minutes = CommonBookmarks.zero(date.getMinutes());
            return {
                date: day + "." + month + "." + date.getFullYear(),
                time: hours + ":" + minutes,
                datetime: day + "." + month + "." + date.getFullYear() + " " + hours + ":" + minutes
            }
        }

        /**
         * Обрезка строка, если строка длинее, чем length, она обрезается и к ней добавляется многоточие
         * @param  {String} str    Начальная строка
         * @param  {Number} length Допустимая длина строка (по умолчанию равна 20)
         * @return {String}        Полученная строка
         */
        function trimString (str, length) {
            var length = length || 20;
            if (str.length > length) {
                return str.substr(0, length) + "…";
            }
            return str;
        }

        return {
            bookid: getBookID,
            chapter: {
                name: getChapterName,
            },
            date: {
                normalize: getNormalizeDate,
                convert: dateConvert,
            },
            format: {
                url: formatUrl,
            },
            getData: getData,
            getPathAndName: getPathAndName,
            page: {
                x: PageX,
                y: PageY,
                position: withParagraph
            },
            string: {
                trim: trimString,
            },
            zero: preZero,
        }
    }();

    /**
     * Приватные функции
     */
    /**
     * Функция инициализации
     */
    var main = function() {
        config.book.id = CommonBookmarks.bookid();
        config.book.data = CommonBookmarks.getData();
        config.book.geturl = CommonBookmarks.format.url("get");
        config.book.saveurl = CommonBookmarks.format.url("save");
        config.book.delurl = CommonBookmarks.format.url("delete");

        $("body").append(DrofaBookmarks.config.tpl.bar);
        DrofaBookmarks.gets();

        captureBar.start( {
            elem: $("body"),
            target: "bookmarksBar",
            checkDown: function(e) { return e.target.id == "bookmarksBar" && DrofaBookmarks.config.link.success },
            checkTarget: function(e) { return e.target.id == "bookmarksBar" },
            funcDo : function(e) {
                var bodyOffset = $("body").offset(),
                    bodyWidth = parseInt(window.getComputedStyle(document.body).width),
                    bodyMarginLeft = parseInt(window.getComputedStyle(document.body).marginLeft),
                    bodyPaddingRight = parseInt(window.getComputedStyle(document.body).paddingRight),
                    espacio = bodyMarginLeft + bodyWidth,
                    espacioLeft = bodyMarginLeft + (bodyWidth - bodyPaddingRight),
                    mouseLeft = captureBar.e.pageX,
                    mouseTop;
                if (captureBar.e.type == "tap" || captureBar.e.offsetY === undefined) {
                    mouseTop = captureBar.e.pageY - DrofaBookmarks.config.corrector;
                } else {
                    mouseTop = captureBar.e.offsetY - DrofaBookmarks.config.corrector;
                }
                DrofaBookmarks.create(mouseTop);
            }
        })
    }

    /**
     * Полоса захвата события click/tap
     * @param {Number}  delay     Продолжительность события click/touchstart
     * @param {[type]}  e         [description]
     * @param {[type]}  timer     [description]
     * @param {[type]}  target    [description]
     * @param {Boolean} dragStop  [description]
     * @method clear             Очистка
     * @method start             Инициализация
     * @method drag              Перетаскивание
     */
    var captureBar = {
        delay: 500,
        e: null,
        timer: null,
        target: null,
        dragStop: true,
        clear: function() {
            if (this.timer !== null) { clearTimeout(this.timer); }
            this.timer = null;
        },
        start: function(opt) {
            opt.elem.on("touchstart", handlerEvent.bind(this))
            .on("mousedown", handlerEvent.bind(this))
            .on("mouseup touchend", function(e) {
               this.clear();
            }.bind(this));

            function handlerEvent(e) {
                if (opt.checkTarget && !opt.checkTarget(e)) { return; }
                if (e.type == "mousedown") {
                    if (self.touchstart == true) { this.touchstart = false; return; }
                }
                this.clear();
                this.e = e;
                this.target = opt.target;
                this.dragStop = false;
                if (e.type !== "mousedown") {
                    this.touchstart = true;
                } else {
                    var timeout = this.delay;
                    if (e.ctrlKey == true) { timeout = 0; }
                }
                if (!opt.checkDown || opt.checkDown(e) === true) {
                    this.timer = setTimeout(function() {
                        if (this.timer === null) { return; }
                        this.timer = null;
                        this.dragStop = true;
                        opt.funcDo(e);
                    }.bind(this), timeout);
                }
            }
        },
        drag : function(e) {
            if (this.dragStop === true) { return false; }
            if (this.timer !== null && e.target.id != this.target) { this.clear(); }
            return true;
        },
    };

    /**
     * Создание закладки
     * @param  {Integer} top Позиция мыши в момент создания закладки
     */
    var createBookmark = function(top) {
        $(DrofaBookmarks.config.wrapper).popover("destroy");
        // Коррекция положения закладки или скроллирования окна, чтобы не попадало под панель навигации
        // DrofaBookmarks.config.topCorrect - высота между кнопкой закрытия и закладкой
        if (top > $("body").outerHeight(true) - DrofaBookmarks.config.topCorrect) {
            $("body").outerHeight(true) - DrofaBookmarks.config.topCorrect;
        }
        if (top > window.scrollY + window.innerHeight - DrofaBookmarks.config.topCorrect) {
            window.scrollTo(window.scrollLeft, top - window.innerHeight + DrofaBookmarks.config.topCorrect);
        }

        var heightNav = $("nav").outerHeight(true) + DrofaBookmarks.config.topCorrect;
        if (top < heightNav) {
            top = heightNav;
        }
        if (top - window.scrollY < heightNav) {
            window.scrollTo(window.scrollLeft, top - heightNav);
        }

        $(DrofaBookmarks.config.popoverclass).remove();
        var position = CommonBookmarks.page.position(top);

        var newBookmark = new DrofaBookmark({pos: position.pos, page: position.page, pagepos: position.pagepos, tag: position.tag});
        var dlg = DrofaBookmarks.config.tpl.popup.replace(/{{BookmarkID}}/gm, newBookmark.id);
        $("#bookmarksBar").append(dlg);
        $("#" + newBookmark.id).css({ "top": top});

        var id = "#" + newBookmark.id;
        $(id).popover({
            html: true,
            placement: "left",
            content: DrofaBookmarks.config.tpl.bookmark
        });
        $(id).popover("show");

        $("#BookmarkForm h4").text("Создать закладку");
        $("#BookmarkSubmit").text("Добавить закладку");

        for (var prop in newBookmark) {
            if (typeof newBookmark[prop] != "function") {
                $("[name=" +  prop +"]").val(newBookmark[prop]);
            }
        }

        $("#BookmarkRemove").show();

        var date_formated = CommonBookmarks.date.convert(newBookmark.date);
        $("#EditBookmarkFootbar .user").html(DrofaBookmarks.config.tpl.data.username.replace("{{username}}", DrofaBookmarks.config.book.data.username));
        $("#EditBookmarkFootbar .date").html(DrofaBookmarks.config.tpl.data.date.replace("{{date}}", date_formated.date));
        $("#EditBookmarkFootbar .time").html(DrofaBookmarks.config.tpl.data.time.replace("{{time}}", date_formated.time));
        $("#EditBookmarkFootbar .creator").html(DrofaBookmarks.config.tpl.data.creator.replace("{{username}}", DrofaBookmarks.config.book.data.username));
        $("#EditBookmarkFootbar .createdate").html(DrofaBookmarks.config.tpl.data.createdate.replace("{{date}}", date_formated.date));
        $("#EditBookmarkFootbar .createtime").html(DrofaBookmarks.config.tpl.data.createtime.replace("{{time}}", date_formated.time));

        CurrentBookmark = newBookmark;
        popoverBinds();
    }

    /**
     * Обработка события submit формы редактирования закладки
     */
    function submitFormBookmark() {
        // Создаем закладку
        $(".bmproperty").each(function(id, param) {
            CurrentBookmark[param.name] = param.value;
        });
        CurrentBookmark.msg = $("#BookmarkMsg").val();
        CurrentBookmark.date = CommonBookmarks.date.normalize();
        CurrentBookmark.username = DrofaBookmarks.config.book.data.username;
        var bookmark = new DrofaBookmark(CurrentBookmark);
        bookmark.save();
        // Убиваем все всплывающие окна
        $(DrofaBookmarks.config.wrapper).popover("destroy");
        $(".popover").remove();
        CurrentBookmark = {};
        // Размечаем закладки
        markBookmarks();
    }

    /**
     * Включение фильтрации закладок
     */
    var activateFilterBookmarks = function() {
        var id = DrofaBookmarks.config.book.id;
        var actual = parseInt(document.getElementById("SelectNoLater").dataset.state) == 1;
        var later = parseInt(document.getElementById("SelectLater").dataset.state) == 1;
        var count = parseInt(document.getElementById("BookmarkCounter").dataset.state) == 1;
        DrofaProperties.set(id + "filter", JSON.stringify({
            "actual": actual,
            "later": later,
            "count": count
        }));
        filterBookmarks();
    }

    /**
     *  Получение параметров фильтрации из Storage
     * @return {Object}     Параметры фильтрации
     */
    var getFiltres = function() {
        var obj = {
            actual: true,
            later: true,
            count: false
        };
        var filtres = DrofaProperties.get(DrofaBookmarks.config.book.id + "filter") || "[]";
        if (filtres != "[]") {
            obj = JSON.parse(filtres);
        }

        var r = {};
        for (var key in obj) {
            r[key] = +!!obj[key];
        }
        return r;
    }

    /**
     * Фильтрация закладок
     */
    var filterBookmarks = function() {
        var obj = getFiltres();
        processFilter(!!obj.actual, "#SelectNoLater", ".noLater");
        processFilter(!!obj.later, "#SelectLater", ".Later");
        processFilter(!!obj.count, "#BookmarkCounter", ".bookMarkNum");

        /**
         * Обработка фильтра
         * @param  {Boolean} value       Значение фильтра
         * @param  {String} viewElement  Указание на элемент DOM, отображающий состояние данного фильтра
         * @param  {String} sortElement  Указание на класс DOM, который скрывается или показывается при фильтрации
         */
        function processFilter(value, viewElement, sortElement) {
            if (value) {
                $(viewElement).removeClass("fa-square-o").addClass("fa-check-square-o");
                $(sortElement).show();
                $(viewElement).data("state", 1);
            } else {
                $(viewElement).removeClass("fa-check-square-o").addClass("fa-square-o");
                $(sortElement).hide();
                $(viewElement).data("state", 0);
            }
        }
    }

    /**
     * Получение одиночной закладки по ее ID
     * @param  {String} id         ID закладки
     * @return {DrofaBookmark}     Объект закладки или null
     */
    var getBookmark = function(id) {
        var data = DrofaProperties.get(DrofaBookmarks.config.book.id) || "{}",
            bookmarks = JSON.parse(data),
            bookmark = new DrofaBookmark(bookmarks[id]) || null;
        return bookmark;
    }

    /**
     * Расстановка закладок на странице
     * @param  {Boolean} all     Обновлять все закладки, по умолчанию true
     */
    var markBookmarks = function(all) {
        if (all == undefined) { all = true; }
        if (typeof DrofaToolbar == "undefined") {
            all = false;
        }
        $(DrofaBookmarks.config.wrapper).remove();
        if (all) { $("#MenuBookmarks li").remove(); }

        var chaptername = CommonBookmarks.chapter.name(),
            filters = getFiltres();
        var rawBookmarks = DrofaProperties.get(DrofaBookmarks.config.book.id);
        if (rawBookmarks === undefined || rawBookmarks === "undefined" || rawBookmarks === "") {
            rawBookmarks = "[]";
        }

        if (rawBookmarks != "[]") {
            if (all) {
                $("#MenuBookmarks").remove();
                var MenuBookmarksContainer = DrofaBookmarks.config.tpl.container.replace("{{filters.actual}}", filters.actual).replace("{{filters.later}}", filters.later).replace("{{filters.count}}", filters.count);
                $("#bmmenu").append(MenuBookmarksContainer);
            }

            $(".laterselect").on(DrofaBookmarks.config.events.click, function(e) {
                if (parseInt(this.dataset.state) == 1) {
                    this.dataset.state = 0;
                } else {
                    this.dataset.state = 1;
                }
                activateFilterBookmarks();
                return false;
            });

            $(".sortbtn").on(DrofaBookmarks.config.events.click,function(e) {
                e.preventDefault();
                var type = $(this).data("type"),
                    sort = $(this).data("sort");
                if (sort == "asc") { sort = "desc"; } else { sort = "asc"; }
                DrofaProperties.set(DrofaBookmarks.config.book.id + "sort", JSON.stringify({
                    "type": type,
                    "sort": sort
                }));
                sortButtons();
                markBookmarks();
                return false;
            });

            var bookmarks = JSON.parse(rawBookmarks);
            bookmarks = sortBookmark(bookmarks);
            sortButtons();

            processBookmarks(bookmarks);

            if (all) {
                $("#MenuBookmarksContainer div.close").on(DrofaBookmarks.config.events.click, function(e) {
                    var oid = $(this).data("id"),
                        el = $("#menu" + oid)[0];
                    if (el === undefined) { return false; }
                    if (confirm("Вы, действительно, хотите удалить закладку?")){
                        var bookmark = getBookmark(oid);
                        if (bookmark !== null) {
                            bookmark.remove();
                        }
                        markBookmarks(false);
                        el.remove();
                    }
                    return false;
                });

                $("a.menuitem").on(DrofaBookmarks.config.events.click, function() {
                    DrofaProperties.set(DrofaBookmarks.config.book.id + "mpos", this.parentElement.id);
                    if (typeof DrofaToolbar !== "undefined") {
                        DrofaToolbar.tohash();
                    }
                });
            }
        } else {
            if (all) {
                $("#MenuBookmarks").remove();
            }
        }

        $(".bookmark").tooltip({ placement: "left" });
        $("#bmmenu").on("hide.bs.dropdown", function() {
            DrofaProperties.set(DrofaBookmarks.config.book.id + "mpos", getCurMenuPosition());
        });
        $("#bmmenu").on("shown.bs.dropdown", function() {
            fn.showBookmarksWindow(DrofaBookmarks.config.book.id + "mpos");
        });

        captureBar.start({
            elem: $("div.bookmark"),
            target: "bookmark",
            checkTarget : function(e) { return $(e.target).hasClass("bookmark") },
            funcDo : function(e) {
                $(DrofaBookmarks.config.wrapper).popover("destroy");
                $(DrofaBookmarks.config.popoverclass).remove();
                clickBookmark(captureBar.e.target.parentElement);
            }
        });
        filterBookmarks();

        /**
         * Обработка закладок
         * @param  {Object} dict Объект, содержащий все закладки
         */
        function processBookmarks(dict) {
            for (var key in dict) {
                var values = {},
                    bookmark = new DrofaBookmark(dict[key]);
                values.id = bookmark.id;
                var msg = bookmark.msg || "";
                if (msg !== "") {
                    msg = msg.replace(/</gim, "&#60;")
                    .replace(/>/gim, "&#62;").replace(/&/gim, "&#38;")
                    .replace(/\"/gim, "&#34;").replace(/[\u0001-\u001F]/gim, "");
                }
                values.msg = msg;
                if (msg === "") { msg = "<i>Без названия</i>"; }
                var arr = msg.split(" ");
                if (arr.length > 2) {
                    msg = arr[0] + " " + arr[1] + "...";
                }

                var PaN = CommonBookmarks.getPathAndName(bookmark);
                values.pagename = PaN.pagename;
                values.path = PaN.path;
                values.color = bookmark.clr || "blue";

                var username = bookmark.username || "Локальный пользователь";
                var creator = bookmark.creator || username;

                var date_formated = CommonBookmarks.date.convert(parseInt(bookmark.date));

                bookmark.createdate = bookmark.createdate || bookmark.date;
                var createdate_formated = CommonBookmarks.date.convert(parseInt(bookmark.createdate));

                values.date = date_formated.date;
                values.time = date_formated.time;
                values.createdate = createdate_formated.date;
                values.createtime = createdate_formated.time;

                values.creator = "Создано: " + creator + " в " + createdate_formated.datetime;
                values.username = "Отредактировано: " + username + " в " + date_formated.datetime;

                values.usernametrim = CommonBookmarks.string.trim(username, 20);
                values.creatortrim = CommonBookmarks.string.trim(creator, 20);

                values.laterclass = " noLater";
                bookmark.later = bookmark.later || 0;
                if (parseInt(bookmark.later) == 1) {
                    values.laterclass = " Later";
                }

                if (all) {
                    var menuitem = DrofaBookmarks.config.tpl.menuitem;
                }

                var bookmarkMark = DrofaBookmarks.config.tpl.bookmarkMark;
                for (var value in values) {
                    if (all) {
                        menuitem = menuitem.replace(RegExp("{{" + value + "}}", "gim"), values[value]);
                    }
                    bookmarkMark = bookmarkMark.replace(RegExp("{{" + value + "}}", "gim"), values[value]);
                }

                if (all) { $("#MenuBookmarksContainer").append(menuitem); }

                var bmChapter = bookmark.chapter.replace(/ /gim, "%20");
                if (bmChapter == chaptername) {
                    var top = bookmark.getPosition(),
                        widthLeft = parseInt(window.getComputedStyle(document.body).marginLeft),
                        left = parseInt(window.getComputedStyle(document.body).width) + 10,
                        notfound = false;

                    if (+top !== 0) {
                        $("#bookmarksBar").append(bookmarkMark);
                        $("#" + bookmark.id).css({"top": top});
                    } else {
                        notfound = true;
                        var pagepos = bookmark.pagepos;
                        if (pagepos !== 0) {
                            var paso = parseInt(window.getComputedStyle(document.body).height) / 100;
                            top = paso * pagepos;
                            $("#bookmarksBar").append(bookmarkMark);
                            $("#" + bookmark.id).css({ "top": top, });
                        } else {
                            if (document.getElementById(DrofaBookmarks.config.notfound) === null) {
                                $("body").append("<div id=\"" + DrofaBookmarks.config.notfound + "\"></div>");
                            }
                            $("#" + DrofaBookmarks.config.notfound).append(bookmarkMark);
                            $("#bookmarksBar").append(bookmarkMark);
                            top = parseInt(window.getComputedStyle(document.body).marginTop) + 10;
                            $("#" + DrofaBookmarks.config.notfound).css({ "top": top});
                        }
                    }

                    if (bookmark.page == "0") {
                        var posobj = CommonBookmarks.page.position(top);
                        bookmark.page = posobj.page;
                        if (bookmark.tag === "") { bookmark.tag = posobj.tag; }
                        if (bookmark.pos === "" || bookmark.pos === "0") { bookmark.pos = posobj.pos; }
                        if (bookmark.pagepos === "" || bookmark.pagepos === "0") { bookmark.pagepos = posobj.pagepos; }
                        bookmark.save();
                    }

                    $(DrofaBookmarks.config.wrapper).drag(function(e,d) {
                        if (captureBar.drag(e) === true) {
                            $(this).css({ top:d.offsetY });
                        }
                    },{distance: 5});

                    $(DrofaBookmarks.config.wrapper).drag("end", stopDragg);
                }
            }
        }
    }

    /**
     * Определение события для click
     * @return {String}    Название события
     */
    function setEventClick() {
        if (device.mobile()) { return "click"; }
        if (device.ios()) { return "tap"; }
        return "click";
    }

    /**
     * Измение цвета закладки
     * @param  {DOMElement} mark Элемент переключения цвета
     */
    var changeColor = function(mark) {
        CurrentBookmark.clr = mark.value;
        var $target = $("#" + CurrentBookmark.id);
        var $mark = $target.find(".bookmark");
        $mark.removeClass("bookmark-red").removeClass("bookmark-yellow").removeClass("bookmark-green").removeClass("bookmark-blue");
        $mark.addClass("bookmark-" + mark.value);
        $("#EditBookmarkColors label.btn").removeClass("active");
        mark.parentNode.className = mark.parentNode.className + " active";
        if(device.mobile() || device.ios() || device.android() || (device.windows() && Drofa.device.istouch())) {
            var overShift = DrofaBookmarks.config.overshifts.windows;
            if (device.mobile()) { overShift = DrofaBookmarks.config.overshifts.mobile; }
            if (device.ios()) { overShift = DrofaBookmarks.config.overshifts.ios; }
            $("#BookmarkMsg").focus(function(){
                $(window).scrollTo(".popover", 500, {
                    axis: "y",
                    over: {"top": overShift},
                    easing: "easeInOutCubic"
                });
            });
        };
    }

    /**
     * Показ кнопок сортировки
     */
    var sortButtons = function() {
        var obj = {
            type: "page",
            sort: "asc"
        };

        var sort = DrofaProperties.get(DrofaBookmarks.config.book.id + "sort") || "[]";
        if (sort != "[]") {
            obj = JSON.parse(sort);
        }
        $(".sortbtn").data("sort", "asc");
        if (obj.type == "page") {
            $(".sortbtn[data-type=\"date\"]").removeClass("fa-date-asc").removeClass("fa-date-desc").addClass("fa-date");
            if (obj.sort == "asc") {
                $(".sortbtn[data-type=\"page\"]").removeClass("fa-page").removeClass("fa-page-desc").addClass("fa-page-asc");
            } else {
                $(".sortbtn[data-type=\"page\"]").removeClass("fa-page").removeClass("fa-page-asc").addClass("fa-page-desc");
                $(".sortbtn[data-type=\"page\"]").data("sort", "desc");
            }
        } else {
            $(".sortbtn[data-type=\"page\"]").removeClass("fa-page-asc").removeClass("fa-page-desc").addClass("fa-page");
            if (obj.sort == "asc") {
                $(".sortbtn[data-type=\"date\"]").removeClass("fa-date").removeClass("fa-date-desc").addClass("fa-date-asc");
            } else {
                $(".sortbtn[data-type=\"date\"]").removeClass("fa-date").removeClass("fa-date-asc").addClass("fa-date-desc");
                $(".sortbtn[data-type=\"date\"]").data("sort", "desc");
            }
        }
    }

    /**
     * Сортировка закладок
     * @param  {Object} bookmarks Объект, содержащий все закладки книги
     */
    var sortBookmark = function(bookmarks) {
        var obj = {
            type: "page",
            sort: "asc"
        };
        var sort = DrofaProperties.get(DrofaBookmarks.config.book.id + "sort") || "[]";
        if (sort != "[]") { obj = JSON.parse(sort); }
        var sorttype = obj.type + obj.sort;
        var bms = [];
        for (var key in bookmarks) {
            bms.push(bookmarks[key]);
        }
        switch (sorttype) {
            case "pagedesc":
                bms.sort(function(a, b) {
                    return parseInt(b.page) - parseInt(a.page);
                });
                break;
            case "dateasc":
                bms.sort(function(a, b) {
                    return parseInt(a.date) - parseInt(b.date);
                });
                break;
            case "datedesc":
                bms.sort(function(a, b) {
                    return parseInt(b.date) - parseInt(a.date);
                });
                break;
            default:
                bms.sort(function(a, b) {
                    return parseInt(a.page) - parseInt(b.page);
                });
                break;
        }
        out = {};
        for (var a = 0, l = bms.length; a < l; a++) {
            var id = bms[a].id;
            out[id] = bms[a];
        }
        return out;
    }

    /**
     * Обработка нажатия на закладку
     * @param  {DOMElement} obj Элемент DOM, отображающий закладку
     */
    var clickBookmark = function(obj) {
        $(DrofaBookmarks.config.wrapper).popover("destroy");
        $(".tooltip").remove();

        var bmarks = DrofaProperties.get(DrofaBookmarks.config.book.id) || "{}";
        var bookmarks = JSON.parse(bmarks);
        var id = $(obj).attr("id");
        var bmData = bookmarks[id] || null;

        $("#" + id).popover({
            html: true,
            placement: "left",
            content: DrofaBookmarks.config.tpl.bookmark
        });
        $("#" + id).popover("show");

        bmData.creator = bmData.creator || bmData.username;
        bmData.createdate = bmData.createdate || bmData.date;

        if (bmData !== null) {
            CurrentBookmark = bmData;
            $("#BookmarkSubmit").text("Изменить закладку");
            $(".bmproperty").each(function(id, param){
                bmData[param.name] = bmData[param.name] || "";
                param.value = bmData[param.name];
            });
            var later = bmData.later || 0;
            if (parseInt(later) == 0) {
                $("#BookmarkLater").removeAttr("checked");
            } else {
                $("#BookmarkLater").attr("checked","checked");
            }
            $("#BookmarkMsg").text(bmData.msg);
            $("#BookmarkEdit").val(1);
        }

        var date_formated = CommonBookmarks.date.convert(new Date(parseInt(bmData.date)));
        var createdate_formated = CommonBookmarks.date.convert(new Date(parseInt(bmData.createdate)));

        $("#EditBookmarkFootbar .user").html(DrofaBookmarks.config.tpl.data.username.replace("{{username}}", DrofaBookmarks.config.book.data.username));
        $("#EditBookmarkFootbar .date").html(DrofaBookmarks.config.tpl.data.date.replace("{{date}}", date_formated.date));
        $("#EditBookmarkFootbar .time").html(DrofaBookmarks.config.tpl.data.time.replace("{{time}}", date_formated.time));
        $("#EditBookmarkFootbar .creator").html(DrofaBookmarks.config.tpl.data.creator.replace("{{username}}", bmData.creator));
        $("#EditBookmarkFootbar .createdate").html(DrofaBookmarks.config.tpl.data.createdate.replace("{{date}}", createdate_formated.date));
        $("#EditBookmarkFootbar .createtime").html(DrofaBookmarks.config.tpl.data.createtime.replace("{{time}}", createdate_formated.time));

        var colorKey = "#colorb";
        switch (bmData.clr) {
            case "red":
                colorKey = "#colorr"; break;
            case "yellow":
                colorKey = "#colory"; break;
            case "green":
                colorKey = "#colorg"; break;
        }

        $("input[name=color]").each(function() {
            $(this).removeAttr("checked");
            $(this).parent("label").removeClass("active");
        });
        $(colorKey).attr("checked", "cheched");
        $(colorKey).parent("label").addClass("active");
        $("#BookmarkRemove").show();
        popoverBinds();
    }

    /**
     * Различные привязки к событиям
     */
    var popoverBinds = function() {
        $(DrofaBookmarks.config.rootid).css("width", $(DrofaBookmarks.config.rootid).css("width"));
        $("body").css("overflow", "hidden");

        // Событие на кнопку закрытия
        $(".btn-close").on(DrofaBookmarks.config.events.click, function(e) {
            e.preventDefault();
            var id = CurrentBookmark.id;
            $(id).popover("destroy");
            var edit = parseInt($("#BookmarkEdit").val());
            if (edit === 0) {
                $(id).remove();
            }
            $(DrofaBookmarks.config.popoverclass).remove();
            $("body").css("overflow", "");
            CurrentBookmark = {};
        });

        // Событие на кнопку удаления закладки
        $("#BookmarkRemove").on(DrofaBookmarks.config.events.click, function(e) {
            var id = CurrentBookmark.id;
            var bookmark = getBookmark(id);
            if (bookmark !== null) {
                bookmark.remove();
            }
        });

        // Событие на кнопку создания/сохранения закладки
        $("#BookmarkForm").on("submit", function(event) {
            event.preventDefault();
            submitFormBookmark();
            CurrentBookmark = {};
        });

        $("#BookmarkSubmit").on(DrofaBookmarks.config.events.click, function(event) {
            event.preventDefault();
            submitFormBookmark();
            CurrentBookmark = {};
        });

        // Событие на скрытие окна редактирования закладки
        $(DrofaBookmarks.config.wrapper).on("hide.bs.popover", function(e) {
            $("body").css("overflow", "");
            $(DrofaBookmarks.config.rootid).css("width", "");
            markBookmarks();
        });

        if (device.mobile() || device.ios() || device.android() || (device.windows() && Drofa.device.istouch())) {
            var overShift = DrofaBookmarks.config.overshifts.windows;
            if (device.mobile()) { overShift = DrofaBookmarks.config.overshifts.mobile; }
            if (device.ios()) { overShift = DrofaBookmarks.config.overshifts.ios; }
            $(DrofaBookmarks.config.wrapper).on("shown.bs.popover", function(e) {
                var elId = this.getAttribute("aria-describedby");
                $(window).scrollTo(DrofaBookmarks.config.popoverclass, 500, {
                    axis: "y",
                    over: {"top": overShift},
                    easing: "easeInOutCubic"
                });
                $("#" + elId + " .arrow").hide();
            });
        }

        // Событие tap на изменение цвета
        $("#EditBookmarkColors label").on("tap", function(e){
            var myTarget = $(this).children("input:first");
            myTarget.prop("checked", true);
            if (device.ios()) {
                changeColor($(this).children("input:first"));
            }
        });

        // Событие на click "позже"
        $("#BookmarkLater").on(DrofaBookmarks.config.events.click, function(e){
            if (this.checked == true) {
                this.value = 1;
            } else {
                this.value = 0;
            }
        });

        // После всего предыдущего устанавливаем фокус в #BookmarkMsg
        $("#BookmarkMsg").focus();
    }

    /**
     * Получение текущей позиции меню
     * @return {String} ID элемента DOM
     */
    var getCurMenuPosition = function() {
        var bms = $("#MenuBookmarksContainer").find(".bm");
        var el = bms.filter(function() {
                return $(this).position().top > 0 && $(this).position().top < 100;
           });
        if (el.length > 0) {
            return el[0].id;
        }
    }

    /**
     * Остановка перетаскивания закладки
     * @param  {[type]} e  [description]
     * @param  {[type]} ui [description]
     */
    var stopDragg = function(e, ui) {
        if (captureBar.dragStop === true) return;
        $(DrofaBookmarks.config.wrapper).popover("destroy");
        $(DrofaBookmarks.config.popoverclass).remove();

        var top = ui.offsetY,
            obj = CommonBookmarks.page.position(top),
            id = this.id,
            bookmark = getBookmark(id);
        if (bookmark !== null) {
            bookmark.pos = obj.pos;
            bookmark.pagepos = obj.pagepos;
            bookmark.tag = obj.tag;
            bookmark.page = obj.page;
            bookmark.date = CommonBookmarks.date.normalize();
            bookmark.username = DrofaBookmarks.config.book.data.username;
            bookmark.save();
        }
        $(this).css("position", "absolute");
        markBookmarks();
    }

    /**
     * Объект закладки
     * Доступные свойства:
     * @param {String}     id         ID закладки
     * @param {Boolean}    actual     Актуальная/неактуальная закладка
     * @param {String}     chapter    Название главы
     * @param {String}     clr        Цвет закладки (blue, green, red, yellow)
     * @param {String}     createdate Дата создания закладки
     * @param {String}     creator    Имя пользователя, создавшего закладку
     * @param {String}     date       Дата изменения закладки
     * @param {String}     msg        Основной текст закладки
     * @param {String}     page       Страница книги, на которой размещена закладка
     * @param {String}     pagepos    Позиция на странице
     * @param {String}     pos        Позиция относительно элемента, к которому привязана закладка
     * @param {String}     tag        Элемент, к которому привязана закладка
     * @param {String}     uid        ID пользователя
     * @param {String}     username   Имя пользователя, изменившего закладку
     */
    var DrofaBookmark = function(_this) {
        var _this = _this || {};
        this.id = _this.id || makeID("bm", 6);
        this.actual = _this.actual || 0;
        this.later = _this.later || 0;
        this.chapter = _this.chapter || CommonBookmarks.chapter.name();
        this.clr = _this.clr || "blue";
        this.createdate = _this.createdate || CommonBookmarks.date.normalize();
        this.creator = _this.creator || config.book.data.username;
        this.date = _this.date || CommonBookmarks.date.normalize();
        this.msg = _this.msg || "";
        this.page = _this.page || "";
        this.pagepos = _this.pagepos || "";
        this.pos = _this.pos || "";
        this.tag = _this.tag || "";
        this.uid = _this.uid || config.book.data.user_id;
        this.username = _this.username || config.book.data.username;

        this.save = bm.save;
        this.remove = bm.remove;

        /**
         * Получение позиции закладки
         * @return {Integer} Позиция закладки в px
         */
        this.getPosition = function() {
            var anchor = document.getElementById(this.tag);
            if (this.tag == "calcFromBody") {
                anchor = document.body;
            }
            if (anchor === null) {
                return null;
            } else {
                if (this.pos == "NaN" || this.pagepos == "NaN") {
                    anchor = document.getElementById("pageBreak" + this.page);
                    if (anchor === null) { return null; }
                    this.pos = 100;
                }
                var top = CommonBookmarks.page.y(anchor),
                    heightPara = parseInt(anchor.offsetHeight),
                    offset = parseInt(this.pos),
                    mas = 0;
                if (offset > 0) {
                    mas = parseInt(window.getComputedStyle(document.body).fontSize);
                }
                var point = heightPara / 100,
                    off = point * offset;
                return (top + off);
            }
        }

        /**
         * Создание ID закладки
         * @param  {String} txt  Начальный текст
         * @param  {Number} len  Длина генерируемого ID (не включает длину начального текста)
         * @return {[type]}      Сгенерированный ID
         */
        function makeID (txt, len) {
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            for (var i = 0; i < len; i++) {
                txt += possible.charAt(Math.floor(Math.random() * possible.length));
            }
            if ($("#" + txt).length) {
                makeID(txt, len);
            }
            return txt;
        }
    }

    return {
        config: config,
        fn: fn,
        create: createBookmark,
        getBookid: CommonBookmarks.bookid,
        gets: fn.getBookmarks,
        color: changeColor,
        init: main,
        mark: markBookmarks,
    }
})();