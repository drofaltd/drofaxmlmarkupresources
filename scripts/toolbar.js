/*
* @Author: vbatushev
* @Date:   2016-01-04 21:04:29
* @Last Modified by:   Vitaly Batushev
* @Last Modified time: 2016-07-20 18:42:03
* @Name Панель инструментов
* @Version 2.1.7
*/

var DrofaToolbar = function(params){
    var $;
    if (typeof jQuery !== "undefined") {
        var $ = jQuery;
    } else {
        console.log("Не обнаружен jQuery");
        document.head.appendChild("<script src=\"//code.jquery.com/jquery-2.1.4.min.js\"></script>");
    }

    var config = {};

    var defaults = {
        rootid: Drofa.config.rootid || "#RootDiv",
        baseFontSize: 21,
        chaptersFolderName: Drofa.config.chaptersFolderName || "chapters",
        xhtml: Drofa.config.doc.xhtml || true,
        documentTitle: Drofa.config.doc.title || document.title,
        steps: {
            minus1: 0.8,
            plus1: 1.1,
            plus2: 1.2,
            plus3: 1.3
        },
        tpl: {
            header: '<header>\
                <nav>\
                    <h1>{{DocumentTitle}}</h1>\
                    <ul class="left" id="topNavLeft">\
                        <li><a href="{{HelpUrl}}" class="icon menu" rel="external"></a></li>\
                        <li><a href="#" id="ToolbarPrev" class="icon prev" rel="external"></a></li>\
                        <li><input id="pagesnav" class="form-control" type="number" min="0" /></li>\
                        <li><a href="#" id="ToolbarNext" class="icon next" rel="external"></a></li>\
                    </ul>\
                    <ul class="right" id="topNavRight">\
                        <li class="dropdown" id="searchmenu">\
                            <a href="#" class="icon search dropdown-toggle" data-toggle="dropdown"></a>\
                            <div id="MenuSearch" role="menu" class="dropdown-menu dropdown-menu-right">\
                                <form id="MenuSearchBar">\
                                    <input type="text" id="sInput" placeholder="Поиск..." />\
                                    <button type="submit">?</button>\
                                    <button type="reset" id="searchClear"><i class="fa fa-times"></i></button>\
                                </form>\
                                <div id="MenuSearchResults">\
                                </div>\
                            </div>\
                        </li>\
                        <li class="dropdown" id="bmmenu">\
                            <a href="#" class="icon bookmarks dropdown-toggle" data-toggle="dropdown"></a>\
                        </li>\
                        <li class="dropdown">\
                            <a href="#" role="button" class="icon menufont dropdown-toggle" data-toggle="dropdown"></a>\
                            <div id="fontMenu" class="dropdown-menu dropdown-menu-right form-horizontal">\
                                <form id="changeFont" class="btn-group" data-toggle="buttons">\
                                    <label class="btn btn-default"><input type="radio" class="fontsize" value="-1" />-1</label>\
                                    <label class="btn btn-default active"><input type="radio" class="fontsize" value="0" />0</label>\
                                    <label class="btn btn-default"><input type="radio" class="fontsize" value="1" />+1</label>\
                                    <label class="btn btn-default"><input type="radio" class="fontsize" value="2" />+2</label>\
                                    <label class="btn btn-default"><input type="radio" class="fontsize" value="3" />+3</label>\
                                </form>\
                            </div>\
                        </li>\
                    </ul>\
                    <div id="alertConnection" class="alert alert-danger" style="position:absolute; top:0px; right:-75px;"><i class="glyphicon lyphicon-ban-circle"></i></div>\
                </nav>\
            </header>',
            search: "<div id=\"sResults\" style=\"display: none\"></div>",
        },
    }

    var main = function(params) {
        config = DrofaUtils.config(params, defaults);
        config.suffix = ".html"; if (config.xhtml) { config.suffix = ".xhtml"; }
        config.rootPath = DrofaToolbar.getroot();

        if (document.body.dataset.title !== undefined) {
            config.documentTitle = document.body.dataset.title;
            config.documentTitle = config.documentTitle.replace(/&/gim, "&#38;");
        }

        DrofaUtils.start();
        addHeader();

        if (typeof spinArr == "undefined") {
            loadSpinScript();
        } else {
            addSpin();
        }

        initialize();
    }

    var loadSpinScript = function() {
        var spinScript = document.getElementById(Drofa.config.spinScriptId);
        if (spinScript !== null) {
            $(spinScript).remove();
        }
        var oHead = document.getElementsByTagName("head")[0];
        var oScript = document.createElement("script");
        oScript.type = "text/javascript";
        oScript.src = "scripts/spinArray.js";
        oHead.appendChild(oScript);
        oScript.onload = addSpin;
    }

    var htmlSpecialChars = {
        '&': '&amp;',
        '"': '&quot;',
        '<': '&lt;',
        '>': '&gt;',
        '\'': '&apos;'
    };

    // Поиск и замена спецсимволов
    function encodeXml(string) {
        return string.replace(/([\&"<>'])/g, function(str, item) {
            return htmlSpecialChars[item];
        });
    };

    var addHeader = function() {
        var vars = {
            // "DocumentTitle": encodeXml(config.documentTitle),
            "DocumentTitle": config.documentTitle,
            "HelpUrl": config.rootPath + "index" + config.suffix,
        }
        var tpl = config.tpl.header;
        for (var v in vars) {
            tpl = tpl.replace("{{" + v + "}}", vars[v]);
        }
        $("body").prepend(tpl);
        checkFontSize();
        $("body").append(config.tpl.search);
        $("#searchmenu").on("shown.bs.dropdown", function () {
            document.getElementById("sInput").focus();
        });
    }

    var checkFontSize = function() {
        var fontVal = parseInt(DrofaProperties.get("DrofaFontSize"));
        $(".fontsize")
            .parent("label")
            .removeClass("active")
            .removeAttr("checked");
        $(".fontsize[value=\"" + fontVal + "\"]")
            .parent("label")
            .addClass("active")
            .attr("checked");
    }

    var addSpin = function() {
        var prev, prevLink, next, nextLink,
            suffix = config.suffix;

        var lf = listSpinFiles(),
            pathname = window.location.pathname,
            paths = pathname.split("/").reverse(),
            current = paths[0].split(/\./)[0];

        if (current === "") {
            current = "index";
        }

        if (pathname.indexOf("/" + config.chaptersFolderName + "/") > -1) {
            current = config.chaptersFolderName + "/" + paths[1] + "/index";
        }

        for (var f = 0, l = lf.length; f < l; f++) {
            if (escape(lf[f]) == current) {
                if (f === 0) {
                    prevLink = escape(lf[lf.length - 1]) + suffix;
                    prev = lf.length - 1;
                } else {
                    prevLink = escape(lf[f - 1]) + suffix;
                    prev = f - 1;
                }
                if (f == lf.length - 1) {
                    nextLink = escape(lf[0]) + suffix;
                    next = "";
                } else {
                    nextLink = escape(lf[f + 1]) + suffix;
                    next = f + 1;
                }
            }
        }

        if (!prevLink) {
            prevLink = "index" + suffix;
        }
        if (!nextLink) {
            nextLink = "index" + suffix;
        }
        $("header nav a.prev").attr("href", config.rootPath + prevLink);
        $("header nav a.next").attr("href", config.rootPath + nextLink);

        pageListing();

        function listSpinFiles() {
            var files = [], file = "";
            if (typeof spinPaths === "undefined") {
                for (var s = 0, l = spinArr.length; s < l; s++) {
                    if (typeof spinArr[s] !== "undefined" && files.indexOf(spinArr[s]) < 0) {
                        files.push(spinArr[s]);
                        file = spinArr[s];
                    }
                }
            } else {
                files = spinPaths;
            }
            return files;
        }

        function pageListing() {
            var top = parseInt($(config.rootid).css("margin-top")) + parseInt(window.getComputedStyle(document.body).fontSize);
            $("#pagesnav").on("change", function() {
                this.value = getNearestNumber(this.value);
                var page = spinArr[this.value];
                var re = new RegExp(page, "gim");
                if (re.test(window.location.href)) {
                    var pagenum = $(this).val();
                    var target = $(config.rootid).find("[data-page-name=\"" + pagenum + "\"]");
                    if (target.length > 0) {
                        var pos = $(target).offset().top - top;
                        $("html, body").animate({ scrollTop: pos }, 1000);
                    } else {
                        viewCurrentPage();
                    }
                } else {
                    window.location.href = config.rootPath + page + config.suffix + "?#page-" + this.value;
                }

                function getNearestNumber(startNum) {
                    if (spinArr[startNum]) return startNum;
                    while (startNum >= 0) {
                        if (spinArr[startNum]) return startNum;
                        startNum--;
                    }
                    while (startNum < spinArr.length) {
                        if (spinArr[startNum]) return startNum;
                        startNum++;
                    }
                    return spinArr.length - 1;
                }
            });
        }
    }

    var stepFont = function(step) {
        switch (parseInt(step)) {
            case 3: curSize = config.steps.plus3; break;
            case 2: curSize = config.steps.plus2; break;
            case 1: curSize = config.steps.plus1; break;
            case -1: curSize = config.steps.minus1; break;
            default: curSize = 1; break;
        }
        $("body").css("font-size", curSize*baseFontSize + "px");
        if (typeof DrofaBookmarks !== "undefined") {
            DrofaBookmarks.mark();
        }
        Drofa.icons.correct(false);
        if (typeof DrofaPages !== "undefined") {
            DrofaPages.draw();
        }

        $("body").trigger("stepfont-after");
        checkFontSize();
    }

    // Обработка хэша адреса
    var processHash = function() {
        var pos, target,
            top = parseInt($(config.rootid).css("margin-top")) + parseInt(window.getComputedStyle(document.body).fontSize),
            hash = window.location.hash;

        if($(hash).length) {
            if (/^#page/.test(hash)) {
                hash = hash.replace(/#page\-/, "");
                if (hash !== 0) {
                    target = $(config.rootid).find("[data-page-name=\"" + hash + "\"]");
                    pos = $(target).offset().top - top;
                    $("html, body").animate({ scrollTop: pos }, 1000);
                }
            }
            else if (/^#para/.test(hash) || /^#obj\-/.test(hash)) {
                pos = $(hash).offset().top - top;
                $("html, body").animate({ scrollTop: pos }, 1000);
            }
            else if (/^#bm/.test(hash)) {
                    pos = $(hash).offset().top - (top * 2);
                    $("html, body").animate({ scrollTop: pos }, 1000);
            }
            else if (/^#\d+/.test(hash)) {
                hash = hash.replace(/#/, "");
                target = $("a[id=\"obj-" + hash + "\"]");
                pos = $(target).offset().top - top;
                $("html, body").animate({ scrollTop: pos }, 0);
            }
            else {
                pos = $(hash).offset().top - top;
                $("html, body").animate({ scrollTop: pos }, 1000);
            }
        }
    }

    var getRootPath = function() {
        var pathname = window.location.pathname;
        if (pathname.indexOf("help" + config.suffix) > - 1) {
            return "../";
        }
        if (pathname.indexOf("/" + config.chaptersFolderName + "/") > - 1) {
            return "../../";
        }
        return "";
    }

    var viewCurrentPage = function() {
        var page = 0, pages = $("div.PageNum").get().sort(sortPages);
        for (let a = 0, l = pages.length; a < l; a++) {
            var el = pages[a];
            var top = $(el).position().top - $(window).scrollTop();
            var point = $("header").height() + 30;
            if (top < point) {
                page = $(el).data("page");
                $("#pagesnav").val(page);
                return false;
            }
        };
        if (page == 0) {
            page = $(pages[pages.length - 1]).data("page");
            if (isNaN(page)) { page = ""; }
            $("#pagesnav").val(page);
        }

        function sortPages(a, b) {
            return $(b).position().top - $(a).position().top;
        }
    }

    var ToolbarSearch = function() {
        var init = function() {
            // Смотрим в localStorage и берем оттуда поисковую строку, которую подсвечиваем и вставляем в форму поиска
            $("#sInput").val(DrofaProperties.get("DrofaSearchString"));
            if ($("#sInput").val() === "" || $("#sInput").val() == " ") {
                $("#searchClear").hide();
            } else {
                $("#searchClear").show();
                wrapSearch();
            }

            // Обработка формы поиска 2
            $("#MenuSearchBar").submit(function() {
                $(config.rootid + " *").unhighlight();
                wrapSearch();
                return false;
            });

            // Обработка нажатия кнопки закрытия формы поиска
            $("#searchClear").on("tap click", function(e) {
                e.preventDefault();
                clearSearch();
            });

            $(document).on("input", "#sInput", function () {
                var $item = $(this),
                value = $item.val();
                if (value === "") {
                    DrofaProperties.set("DrofaSearchString", value);
                    $(config.rootid + " *").unhighlight();
                }
            });
        }

        var clearSearch = function() {
            $("#sInput").val("");
            $("#MenuSearchResults").html("");
            DrofaProperties.set("DrofaSearchString","");
            $(config.rootid + " *").unhighlight();
            $("#searchClear").hide();
        }

        var wrapSearch = function() {
            var d = new Date();
            $("#searchClear").hide();
            $("#sInput").after("<i id=\"SearchSpinner\" class=\"fa fa-spinner fa-spin\" style=\"font-size: 17px;\"></i>");
            if (document.getElementById("TextBaseScript") === null) {
                DrofaUtils.insertScript("TextBaseScript", "../../scripts/textBase.js", "scripts/textBase.js");
            } else {
                searchInBase();
            }
            $("#SearchSpinner").remove();
        }

        function searchInBase() {
            $(config.rootid + " *").unhighlight();
            var q = $("#MenuSearchBar input").val();
            if (q === "") { clearSearch(); return false; }
            var out = "<p>Ничего не найдено</p>";
            var results = "";
            $.each(textBase, function(i, v) {
                var test = new RegExp(q, "gim");
                var fulltext = v.fulltext;
                fulltext = unescape(fulltext);
                if (test.test(fulltext)) {
                    var re = new RegExp("(" + q + ")", "i");
                    fulltext = fulltext.replace(re, "<span class=\"searchResult\">$1</span>");
                    if (q === undefined) { q = ""; }
                    DrofaProperties.set("DrofaSearchString", q);
                    var pageblock = "";
                    if (v.page !== undefined) {
                        pageblock = "<span class=\"SearchPage\">" + v.page + "</span>";
                    }
                    var linkpage = v.document;
                    if (config.xhtml) { linkpage = linkpage.replace(/\.html/, ".xhtml"); }
                    results += "<li><a class=\"sResult\" data-ajax=\"false\" data-hash=\"" + v.paragraph + "\" href=\"" + config.rootPath + linkpage + "?" + "#" + v.paragraph + "\" onclick=\"$.fancybox.close();\" >" + fulltext + "</a>" + pageblock + "</li>";
               }
            });
            if (results !== "") {
                out = "<ol class=\"searchResults\">" + results + "</ol>";
            }
            $("#MenuSearchResults").html(out);
            $(config.rootid + " *").highlight(DrofaProperties.get("DrofaSearchString"));
            $("#searchClear").show();
        }

        return {
            init: init,
            wrap: wrapSearch,
            search: searchInBase
        }
    }();

    var DrofaUtils = function(){
        var startInit = function() {
            var spinScript = document.querySelector("script[src=\"scripts/spinArray.js\"]");
            if (spinScript !== null) { $(spinScript).remove(); }
        }

        var insertScript = function(id, path, altpath) {
            var script = tryInsert(id, path);
            script.onerror = function() {
                $(script).remove();
                script = tryInsert(id, altpath);
                return true;
            }
        }

        function tryInsert(id, path) {
            var script = document.createElement("script");
            script.type = "text/javascript";
            script.src = path;
            script.id = id;
            script.async = false;
            return document.body.appendChild(script);
        }

        var unique = function(arr) {
            var result = [];
            nextInput:
            for (var i = 0; i < arr.length; i++) {
                var str = arr[i];
                for (var j = 0; j < result.length; j++) {
                    if (result[j] == str) continue nextInput;
                }
                result.push(str);
            }
            return result;
        }

        var cfgMerge = function(params, cfg) {
            for (var param in params) {
                cfg[param] = params[param];
            }
            return cfg;
        }

        return {
            insertScript: insertScript,
            start: startInit,
            unique: unique,
            config: cfgMerge
        }
    }();

    var initialize = function() {

        $(".fontsize").change(function(e){
            e.stopPropagation(); e.preventDefault();
            $(".fontsize").each(function(index, el) { el.checked = false; });
            this.checked = true;
            var step = parseInt($(this).val());
            DrofaProperties.set("DrofaFontSize", step);
            DrofaToolbar.step(step);
        });

        $("#changeFont btn").on("tap click", function(e){
            e.stopPropagation();
            e.preventDefault();
        });

        ToolbarSearch.init();

        // События на изменение хэша в адресе
        $(window).bind("hashchange", function() {
            DrofaToolbar.tohash();
        });

        // События на загрузку окна
        $(window).load(function() {
            baseFontSize = parseInt(window.getComputedStyle(document.body).fontSize);
            var lsFS = DrofaProperties.get("DrofaFontSize");
            if (lsFS === undefined) {
                DrofaProperties.set("DrofaFontSize", 0);
            }

            DrofaToolbar.step(DrofaProperties.get("DrofaFontSize"));
            DrofaToolbar.tohash();      // Обрабатываем хэш адреса
            DrofaToolbar.topage();  // Отображаем в тулбаре текущую страницу
        });

        // События на изменение окна
        $(window).resize(function(){
            if (typeof DrofaBookmarks !== "undefined" && document.getElementsByClassName("bookmark").length == 0) {
                DrofaBookmarks.mark();    // Расставляем закладки
            }
        });

        // События на промотку окна
        $(window).scroll(function() {
            DrofaToolbar.topage();
        });

        document.onkeydown = function(event) {
            if (!document.getElementById) { return; }
            if (window.event) { event = window.event; }

            if (event.altKey && !event.shiftKey) {
                var link = null;
                var href = null;
                switch (event.keyCode ? event.keyCode : event.which ? event.which : null) {
                    case 0x25:
                        link = document.getElementById ("ToolbarPrev");
                        break;
                    case 0x27:
                        link = document.getElementById ("ToolbarNext");
                        break;
                }
                if (link && link.href) document.location = link.href;
                if (href) document.location = href;
            }
            if (event.keyCode == 27) {
                $(".popover").remove();
                $("body").css("overflow-y", "auto");
            }
        }
    }

    return {
        init: main,
        step: stepFont,
        tohash: processHash,
        topage: viewCurrentPage,
        getroot: getRootPath,
        search: ToolbarSearch.search
    }
}();

function searchInBase() {
    DrofaToolbar.search();
}